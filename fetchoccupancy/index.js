import { NodeSSH } from 'node-ssh';
import PocketBase from 'pocketbase';
import * as dotenv from 'dotenv';
dotenv.config();

const fetchOccupiedMachines = async () => {
	const ssh = new NodeSSH();
	await ssh.connect({
		host: process.env.SSH_HOST,
		username: process.env.SSH_USERNAME,
		password: process.env.SSH_PASSWORD,
		port: 22
	});
	const ruptime_output = await ssh.execCommand('ruptime -a').then(({ stdout }) => stdout);
	ssh.dispose();
	return ruptime_output
		.split('\n')
		.filter((line) => line.match(/[1-9]+[0-9]* user/))
		.map((line) => line.split(' ')[0]);
};

const countOccupiedMachinesPerRoom = (occupiedMachines) => {
	const rooms = {
		cip: [
			'c142',
			'c143',
			'c144',
			'c145',
			'c146',
			'c147',
			'c148',
			'c149',
			'c150',
			'c151',
			'c152',
			'c153',
			'c154',
			'c155',
			'c156',
			'c157',
			'c158',
			'c159',
			'c160',
			'c161',
			'c162',
			'c163',
			'c164',
			'c165',
			'c166',
			'c167'
		]
	};

	return Object.fromEntries(
		Object.entries(rooms).map(([roomName, hostnames]) => [
			roomName,
			occupiedMachines.filter((hostname) => hostnames.includes(hostname)).length
		])
	);
};

const submitToDatabase = async (occupiedMachinesPerRoom) => {
	const pb = new PocketBase(process.env.DB_HOST);
	await pb.admins.authWithPassword(process.env.DB_USERNAME, process.env.DB_PASSWORD);

	const records = Object.entries(occupiedMachinesPerRoom).map(([room, machinesOccupied]) => ({
		room,
		machinesOccupied
	}));

	await Promise.all(records.map((record) => pb.collection('occupancyHistory').create(record)));
	pb.authStore.clear();
};

function update() {
	fetchOccupiedMachines().then(countOccupiedMachinesPerRoom).then(submitToDatabase);
	setTimeout(update, 5 * 60 * 1000);
}
update();
